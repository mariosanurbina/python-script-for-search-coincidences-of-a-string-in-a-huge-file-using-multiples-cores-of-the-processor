import multiprocessing as mp,os

def process(linea):

    encontrar = "string you want to find";
    if linea.find(encontrar):
    	pass
    else:
        print(linea)
        
def process_wrapper(chunkStart, chunkSize):
    with open("fileyouwanttoanalyse.txt") as f:
        f.seek(chunkStart)
        lines = f.read(chunkSize).splitlines()
        for line in lines:
            process(line)

def chunkify(fname,size=1024*1024):
    fileEnd = os.path.getsize(fname)
    with open(fname,'r') as f:
        chunkEnd = f.tell()
        while True:
            chunkStart = chunkEnd
            f.seek(size,1)
            f.readline()
            chunkEnd = f.tell()
            yield chunkStart, chunkEnd - chunkStart
            if chunkEnd > fileEnd:
                break

#init objects
pool = mp.Pool(2)
jobs = []

#create jobs
for chunkStart,chunkSize in chunkify("fileyouwanttoanalyse.txt "):
    jobs.append( pool.apply_async(process_wrapper,(chunkStart,chunkSize)) )

#wait for all jobs to finish
for job in jobs:
    job.get()

#clean up
pool.close()
